import { Component, OnInit, Input } from "@angular/core";
import { Task } from "./../column/task";
import { TASKS } from "./../mock-tasks";
import { Validators, FormControl, FormGroup } from "@angular/forms";

@Component({
  selector: "app-task-card",
  templateUrl: "./task-card.component.html",
  styleUrls: ["./task-card.component.scss"]
})
export class TaskCardComponent implements OnInit {
  @Input() task: Task;
  @Input() color: string;
  tasks = TASKS;
  taskForm: FormGroup;
  constructor() {}

  ngOnInit() {
    this.taskForm = new FormGroup({
      taskName: new FormControl("", {
        validators: Validators.required,
        updateOn: "submit"
      }),
      column: new FormControl("", {
        validators: Validators.required,
        updateOn: "submit"
      }),
      status: new FormControl("", {
        validators: Validators.required,
        updateOn: "submit"
      })
    });
  }
  columns = ["Design Area", "Frontend Area", "Development Area", "Board Name"];
  statuses = ["in Progress", "done"];

  onSubmit(editModal) {
    if (this.taskForm) {
      let selected_task = this.tasks.find(task => {
        return task.id === this.task.id;
      });
      selected_task = { ...selected_task, ...this.taskForm.value };
      console.log(selected_task, this.taskForm);
    }
    editModal.hide();
    this.taskForm.reset();
  }
}
