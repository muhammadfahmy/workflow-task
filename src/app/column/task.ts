export class Task {
  constructor(
    public id: number,
    public taskName: string,
    public column: string,
    public status: string
  ) {}
}
