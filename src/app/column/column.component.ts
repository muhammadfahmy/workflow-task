import { Component, OnInit, Input } from "@angular/core";
import { Task } from "./task";
import { TASKS } from "./../mock-tasks";

@Component({
  selector: "app-column",
  templateUrl: "./column.component.html",
  styleUrls: ["./column.component.scss"]
})
export class ColumnComponent implements OnInit {
  @Input() name: string;
  @Input() color: string;
  tasks = TASKS;
  taskName: String;
  constructor() {}

  ngOnInit() {
    this.taskName = "";
  }
  columns = ["Design Area", "Frontend Area", "Development Area", "Board Name"];

  onSubmit(taskForm, basicModal) {
    if ((taskForm.submitted = true)) {
      this.tasks.push({
        id: this.tasks.length + 1,
        column: this.name,
        status: "in Progress",
        ...taskForm.form.value
      });
      console.log(taskForm);
    }

    basicModal.hide();
    taskForm.reset();
  }
}
