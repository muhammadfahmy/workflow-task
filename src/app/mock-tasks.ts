import { Task } from "./column/task";

export const TASKS: Task[] = [
  {
    id: 1,
    taskName: "Task 1",
    column: "Design Area",
    status: "in Progress"
  },
  {
    id: 2,
    taskName: "Task 2",
    column: "Frontend Area",
    status: "in Progress"
  },
  {
    id: 3,
    taskName: "Task 3",
    column: "Development Area",
    status: "in Progress"
  },
  {
    id: 4,
    taskName: "Task 4",
    column: "Board Name",
    status: "in Progress"
  }
];
