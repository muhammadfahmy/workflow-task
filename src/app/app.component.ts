import { Component, Input } from "@angular/core";
import { TASKS } from "./mock-tasks";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent {
  title = "workflow";
  @Input() color: string;
  tasks = TASKS;
  taskName: String;
  constructor() {}

  ngOnInit() {
    this.taskName = "";
  }
  columns = ["Design Area", "Frontend Area", "Development Area", "Board Name"];

  onSubmit(taskForm, basicModal) {
    if ((taskForm.submitted = true)) {
      this.tasks.push({
        id: this.tasks.length + 1,
        status: "in Progress",
        ...taskForm.form.value
      });
      console.log(taskForm);
    }

    basicModal.hide();
    taskForm.reset();
  }
}
